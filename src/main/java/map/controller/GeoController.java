package map.controller;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.Single;
import map.common.LatLng;
import map.common.Places;
import map.service.GeoService;

@Controller
public class GeoController {

    private final GeoService geoService;

    public GeoController(GeoService geoService) {
        this.geoService = geoService;
    }

    @Get("/places/{place}")
    public Single<LatLng> getLatLng(Places place) {
        return geoService.getLatLng(place);
    }
}